﻿using System;

namespace ParrotWings.Infrastructure.Configuration
{
    /// <summary>
    /// Marks class as entity object for mapping to database
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class EntityAttribute : Attribute
    {

    }
}
