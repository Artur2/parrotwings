﻿using System.Data.Entity.Migrations;
using ParrotWings.Infrastructure.Repositories.UnitOfWork;

namespace ParrotWings.Infrastructure.Configuration
{
    public class DatabaseConfiguration : DbMigrationsConfiguration<UnitOfWork>
    {
        public DatabaseConfiguration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = false;
        }
    }
}
