﻿using System.Linq;
using ParrotWings.Infrastructure.Repositories.UnitOfWork;

namespace ParrotWings.Infrastructure.Repositories
{
    public interface IRepository<TEntity> where TEntity : class
    {
        TEntity Get(object id);

        void Add(TEntity entity);

        void Remove(object id);

        IQueryable<TEntity> AsQueryable();

        IUnitOfWork UnitOfWork { get; }
    }
}
