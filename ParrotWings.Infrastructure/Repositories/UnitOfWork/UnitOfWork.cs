﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using ParrotWings.Infrastructure.Configuration;

namespace ParrotWings.Infrastructure.Repositories.UnitOfWork
{
    public class UnitOfWork : DbContext, IQueryableUnitOfWork
    {
        public UnitOfWork()
        {

        }

        public virtual void Attach<TEntity>(TEntity entity) where TEntity : class
        {
            GetSet<TEntity>().Attach(entity);
        }

        public virtual void Commit()
        {
            SaveChanges();
        }

        public virtual IDbSet<TEntity> GetSet<TEntity>() where TEntity : class
        {
            return base.Set<TEntity>();
        }

        public virtual void Rollback()
        {
            foreach (var entry in ChangeTracker.Entries())
            {
                if (entry.State == EntityState.Added)
                    entry.State = EntityState.Detached;
                else if (entry.State == EntityState.Deleted)
                    entry.State = EntityState.Unchanged;
                else if (entry.State == EntityState.Modified)
                    entry.State = EntityState.Unchanged;
            }

            SaveChanges();
        }

        private Lazy<List<Type>> EntitiesToRegister => new Lazy<List<Type>>(() =>
        {
            var types = new List<Type>();
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();
            foreach (var assembly in assemblies.Where(item => item.FullName.Contains("ParrotWings")))
            {
                types.AddRange(assembly.GetTypes()
               .Where(type => type.GetCustomAttributes(typeof(EntityAttribute), true).Any()));
            }

            return types;
        });

        private MethodInfo genericRegistrationMethodInfo = typeof(UnitOfWork).GetMethods(BindingFlags.Instance | BindingFlags.NonPublic)
            .FirstOrDefault(method => method.Name == "RegisterType");

        private void RegisterType<TEntity>(DbModelBuilder modelBuilder) where TEntity : class
        {
            modelBuilder.Entity<TEntity>();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //For more complex and free logic add IEntityConfigurationFactory with method CreateUsingEntityFramework
            // and do some codding =))
            foreach (var item in EntitiesToRegister.Value)
            {
                genericRegistrationMethodInfo.MakeGenericMethod(item)
                    .Invoke(this, new[] { modelBuilder });
            }

            base.OnModelCreating(modelBuilder);
        }
    }
}
