﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using AutoMapper;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using ParrotWings.Domain.Entities.IdentityAgg;
using ParrotWings.Domain.Entities.TransactionAgg;
using ParrotWings.Infrastructure.Repositories;
using ParrotWings.Web.Api.DTO_s;

namespace ParrotWings.Web.Api.v1
{
    [Authorize]
    [RoutePrefix("api/v1/identityusers")]
    public class IdentityUsersController : ApiController
    {
        private IRepository<IdentityUser> _identityUsersRepository;
        private IRepository<Transaction> _transactionsRepository;
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public IdentityUsersController(
            IRepository<IdentityUser> identityUsersRepository,
            IRepository<Transaction> transactionsRepository)
        {
            _identityUsersRepository = identityUsersRepository;
            _transactionsRepository = transactionsRepository;
        }

        public ApplicationSignInManager SignInManager => _signInManager ??
            (_signInManager = HttpContext.Current.GetOwinContext().Get<ApplicationSignInManager>());

        public IAuthenticationManager AuthenticationManager => HttpContext.Current.GetOwinContext().Authentication;

        public ApplicationUserManager UserManager => _userManager ??
            (_userManager = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>());

        [HttpGet]
        [Route("")]
        public IEnumerable<IdentityUserDTO> List()
        {
            return _identityUsersRepository.AsQueryable().Select(Mapper.Map<IdentityUser, IdentityUserDTO>);
        }

        [HttpGet]
        [Route("current")]
        [AllowAnonymous]
        public IHttpActionResult Current()
        {
            if (!User.Identity.IsAuthenticated)
                return BadRequest();

            var currentUser = _identityUsersRepository.AsQueryable()
                .FirstOrDefault(user => user.Email == User.Identity.Name);

            if (currentUser == null)
                return BadRequest("User is null");

            return Ok(Mapper.Map<IdentityUser, CurrentIdentityUserDTO>(currentUser));
        }

        [HttpGet]
        [Route("login", Name = "login")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> Login([FromUri]IdentityUserLoginDTO model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var authenticatedUser = await UserManager.FindByEmailAsync(model.Email);
            if (authenticatedUser == null)
                return Json(false);

            var result = await SignInManager.PasswordSignInAsync(authenticatedUser.UserName, model.Password, isPersistent: true, shouldLockout: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return Ok();
                default:
                    ModelState.AddModelError("", "Invalid login attempt.");
                    return BadRequest(ModelState);
            }
        }

        [HttpGet]
        [Route("logout")]
        public IHttpActionResult Logout()
        {
            AuthenticationManager.SignOut();

            return Ok();
        }

        [HttpGet]
        [Route("{id}", Name = "user")]
        public IHttpActionResult Get(string id)
        {
            var identityUser = _identityUsersRepository.Get(id);
            if (identityUser == null)
                return StatusCode(HttpStatusCode.NotFound);

            return Ok(Mapper.Map<IdentityUser, IdentityUserDTO>(identityUser));
        }

        [HttpGet]
        [Route("{id}/sendedtransactions")]
        public IEnumerable<TransactionDTO> SendedTransactions(string id)
        {
            return _transactionsRepository.AsQueryable()
                .Where(transaction => transaction.SenderId == id)
                .Select(Mapper.Map<Transaction, TransactionDTO>);
        }

        [HttpGet]
        [Route("{id}/receivedtransactions")]
        public IEnumerable<TransactionDTO> ReceivedTransactions(string id)
        {
            return _transactionsRepository.AsQueryable()
                .Where(transaction => transaction.ReceiverId == id)
                .Select(Mapper.Map<Transaction, TransactionDTO>);
        }

        [HttpPost]
        [Route("")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> Create([FromBody]NewIdentityUserDTO model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var identityUser = new IdentityUser
            {
                Id = Guid.NewGuid().ToString(),
                UserName = model.UserName,
                Email = model.Email,
                PWBallance = 500
            };

            identityUser.PasswordHash = UserManager.PasswordHasher.HashPassword(model.Password);

            await UserManager.CreateAsync(identityUser);

            var result = await SignInManager.PasswordSignInAsync(model.UserName, model.Password, isPersistent: true, shouldLockout: false);

            switch (result)
            {
                case SignInStatus.Success:
                    return CreatedAtRoute("user", new { id = identityUser.Id }, Mapper.Map<IdentityUser, IdentityUserDTO>(identityUser));
                default:
                    ModelState.AddModelError("", "Invalid login attempt.");
                    return BadRequest(ModelState);
            }
        }
    }
}
