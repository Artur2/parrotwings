﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using ParrotWings.Domain.Entities.IdentityAgg;
using ParrotWings.Domain.Entities.TransactionAgg;
using ParrotWings.Domain.Services;
using ParrotWings.Infrastructure.Repositories;
using ParrotWings.Web.Api.DTO_s;

namespace ParrotWings.Web.Api.v1
{
    [Authorize]
    [RoutePrefix("api/v1/transactions")]
    public class TransactionsController : ApiController
    {
        private IRepository<Transaction> _transactionsRepository;
        private IRepository<IdentityUser> _identityUsersRepository;
        private IPaymentService _paymentService;

        public TransactionsController(
            IRepository<Transaction> transactionsRepostiory,
            IRepository<IdentityUser> identityUsersRepository,
            IPaymentService paymentService)
        {
            _transactionsRepository = transactionsRepostiory;
            _identityUsersRepository = identityUsersRepository;
            _paymentService = paymentService;
        }

        [HttpGet]
        [Route("", Name = "TransactionsList")]
        public IEnumerable<TransactionDTO> List()
        {
            var currentUser = GetCurrentUser();
            return _transactionsRepository
                .AsQueryable()
                .Where(transaction =>
                transaction.SenderId == currentUser.Id)
                .ToList()
                .Select(transaction => Mapper.Map<Transaction, TransactionDTO>(transaction,
                opts =>
                {
                    opts.AfterMap((source, destination) =>
                    {
                        destination.ResultingBalance = source.SenderBalance;
                    });
                }));
        }

        [HttpGet]
        [Route("{id}", Name = "Transaction")]
        public IHttpActionResult Get(string id)
        {
            var transaction = _transactionsRepository
                .AsQueryable()
               .Select(Mapper.Map<Transaction, TransactionDTO>)
               .FirstOrDefault(item => item.Id == id);
            if (transaction == null)
                return StatusCode(HttpStatusCode.NotFound);

            return Ok(transaction);
        }

        [HttpGet]
        [Route("{id}/sender")]
        public IHttpActionResult GetSender(string id)
        {
            var transaction = _transactionsRepository
                .AsQueryable()
               .FirstOrDefault(item => item.Id == id);
            if (transaction == null)
                return StatusCode(HttpStatusCode.NotFound);

            return Ok(Mapper.Map<IdentityUser, IdentityUserDTO>(transaction.Sender));
        }

        [HttpGet]
        [Route("{id}/receiver")]
        public IHttpActionResult GetReceiver(string id)
        {
            var transaction = _transactionsRepository
                .AsQueryable()
               .FirstOrDefault(item => item.Id == id);
            if (transaction == null)
                return StatusCode(HttpStatusCode.NotFound);

            return Ok(Mapper.Map<IdentityUser, IdentityUserDTO>(transaction.Receiver));
        }

        [HttpPost]
        [Route("")]
        public HttpResponseMessage Add([FromBody]TransactionDTO item)
        {
            try
            {
                var newTransaction = _paymentService.CreateTransaction(item.SenderId, item.ReceiverId, item.Amount);
                return Request.CreateResponse(HttpStatusCode.Created, Url.Route("Transaction", new { id = newTransaction.Id }));
            }
            catch (InvalidOperationException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        private IdentityUser GetCurrentUser()
        {
            return _identityUsersRepository.AsQueryable()
                .FirstOrDefault(user => user.Email == User.Identity.Name);
        }
    }
}