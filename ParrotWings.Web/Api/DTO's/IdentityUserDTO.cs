﻿using System;

namespace ParrotWings.Web.Api.DTO_s
{
    public class IdentityUserDTO
    {
        public Guid Id { get; set; }

        public string UserName { get; set; }

        public string Email { get; set; }
    }
}