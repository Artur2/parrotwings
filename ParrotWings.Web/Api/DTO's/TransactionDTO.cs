﻿using System;

namespace ParrotWings.Web.Api.DTO_s
{
    public class TransactionDTO
    {
        public string Id { get; set; }

        public string SenderId { get; set; }

        public string ReceiverId { get; set; }

        public decimal Amount { get; set; }

        public string CorrespondentName { get; set; }

        public DateTime? Date { get; set; }

        public decimal ResultingBalance { get; set; }
    }
}