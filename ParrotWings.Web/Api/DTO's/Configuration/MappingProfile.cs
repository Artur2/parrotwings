﻿using AutoMapper;
using ParrotWings.Domain.Entities.IdentityAgg;
using ParrotWings.Domain.Entities.TransactionAgg;

namespace ParrotWings.Web.Api.DTO_s.Configuration
{
    public class MappingProfile : Profile
    {
        public override string ProfileName => "DefaultProfile";

        protected override void Configure()
        {
            CreateMap<TransactionDTO, Transaction>();
            CreateMap<Transaction, TransactionDTO>();

            CreateMap<IdentityUser, IdentityUserDTO>();
            CreateMap<IdentityUser, CurrentIdentityUserDTO>();
        }
    }
}