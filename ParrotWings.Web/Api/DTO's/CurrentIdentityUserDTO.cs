﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ParrotWings.Web.Api.DTO_s
{
    public class CurrentIdentityUserDTO : IdentityUserDTO
    {
        public decimal PWBallance { get; set; }
    }
}