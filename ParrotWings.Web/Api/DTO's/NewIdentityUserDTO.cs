﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using ParrotWings.Domain.Entities.IdentityAgg;
using ParrotWings.Infrastructure.Repositories;
using CompareAttribute = System.ComponentModel.DataAnnotations.CompareAttribute;

namespace ParrotWings.Web.Api.DTO_s
{
    public class NewIdentityUserDTO : IValidatableObject
    {
        [Required]
        public string UserName { get; set; }

        [EmailAddress]
        [Required]
        public string Email { get; set; }

        [DataType(DataType.Password)]
        [Required]
        public string Password { get; set; }

        [Compare("Password")]
        [Required]
        public string PasswordConfirm { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var identityUsersRepository = DependencyResolver.Current.GetService<IRepository<IdentityUser>>();
            if (identityUsersRepository.AsQueryable().Any(user => user.Email == Email))
                yield return new ValidationResult("Такой Email уже существует в системе",new[] { "Email" });
        }
    }
}