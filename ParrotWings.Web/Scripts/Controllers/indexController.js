﻿var parrotWingsIndexApp = angular.module('ParrotWingsIndexControllers', ['ParrotWingsIdentityService', 'ParrotWingsCurrentIdentityService']);

parrotWingsIndexApp.controller('LoginCtrl', function ($scope, $http, currentIdentityService, identityService) {

    $scope.init = function () {
        currentIdentityService.redirect();
    }

    $scope.login = function () {
        identityService.login($scope.login.emailAddress, $scope.login.password);
    }
});

parrotWingsIndexApp.controller('RegisterCtrl', function ($scope, $http, $timeout, currentIdentityService, identityService) {

    $scope.init = function () {
        currentIdentityService.redirect();
    }

    $scope.validationMessage = '';

    $scope.register = function () {
        identityService.register(
            $scope.register.userName,
            $scope.register.emailAddress,
            $scope.register.password,
            $scope.register.passwordConfirm,
            function (response) {

                if (response.data.ModelState) {
                    if (response.data.ModelState['model.Email']) {
                        $scope.validationMessage += response.data.ModelState['model.Email'][0];
                    }
                    if (response.data.ModelState['model.PasswordConfirm']) {
                        $scope.validationMessage += response.data.ModelState['model.PasswordConfirm'][0];
                    }
                }

                $timeout(function () {
                    $scope.validationMessage = '';
                }, 2000);
            })
    }
});