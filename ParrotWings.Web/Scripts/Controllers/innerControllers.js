﻿var parrotWingsInnerApp = angular.module('ParrotWingsInnerControllers', ['ParrotWingsCurrentIdentityService', 'ParrotWingsRoutingService']);

parrotWingsInnerApp.controller('TransactionsCtrl', function ($scope, $http) {

    $scope.transactions = [];

    $scope.loadTransactions = function () {
        $http.get('/api/v1/transactions')
        .then(function (response) {
            $scope.transactions = response.data;
        });
    }
});

parrotWingsInnerApp.controller('NavbarCtrl', function ($scope, $http, $interval, currentIdentityService) {

    $scope.pwballance = 0;

    $scope.init = function () {
        $scope.pwballance = currentIdentityService.get().PWBallance;
        $scope.userName = currentIdentityService.get().UserName;
        $interval(function () {
            $scope.pwballance = currentIdentityService.get().PWBallance;
        }, 1000)

        $interval(function () {
            currentIdentityService.refreshLocalStorage();
        }, 10000);
    }

    $scope.logout = function () {
        currentIdentityService.logout();
    };
});

parrotWingsInnerApp.controller('AddTransactionCtrl', function ($scope, $http, $timeout, routingService, currentIdentityService) {

    $scope.identityUsers = [];
    $scope.validationMessage = '';

    $scope.init = function () {
        $http.get('/api/v1/identityUsers')
        .then(function (response) {
            $scope.identityUsers = response.data;
            currentIdentityService.refreshLocalStorage();
        })
    }

    $scope.add = function () {
        var currentUser = JSON.parse(window.localStorage['identity']);
        $http.post('/api/v1/transactions', { ReceiverId: $scope.receiverId, SenderId: currentUser.Id, Amount: $scope.amount })
        .then(function (response) {
            routingService.changeRoute($scope, '#/transactions');
            currentIdentityService.refreshLocalStorage();
        }, function (response) {
            $scope.validationMessage = response.data.Message;

            $timeout(function () {
                $scope.validationMessage = '';
            }, 2000);
        });
    }
})