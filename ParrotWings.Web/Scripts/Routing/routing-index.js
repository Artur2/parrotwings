﻿var parrotWingsIndexApp = angular.module('ParrotWingsIndex', ['ngRoute', 'ParrotWingsIndexControllers']);

parrotWingsIndexApp.config(['$routeProvider',
  function ($routeProvider) {
      $routeProvider.
        when('/login', {
            templateUrl: '/Templates/LoginForm.html',
            controller: 'LoginCtrl'
        }).
        when('/register', {
            templateUrl: '/Templates/RegisterForm.html',
            controller: 'RegisterCtrl'
        }).
        otherwise({
            redirectTo: '/login'
        });
  }]);