﻿var parrotWingsInnerApp = angular.module('ParrotWingsInner', ['ngRoute', 'ParrotWingsInnerControllers']);

parrotWingsInnerApp.config(['$routeProvider',
  function ($routeProvider) {
      $routeProvider.
        when('/transactions', {
            templateUrl: '/Inner/Templates/Transactions.html',
            controller: 'TransactionsCtrl'
        }).
        when('/transactions/add', {
            templateUrl: '/Inner/Templates/AddTransactionForm.html',
            controller: 'AddTransactionCtrl'
        }).
        when('/transactions/:transactionId', {
            templateUrl: '/Inner/Templates/Transaction.html',
            controller: 'TransactionsCtrl'
        })
        .
        otherwise({
            redirectTo: '/transactions'
        });
  }]);