﻿var parrotWingsApp = angular.module('ParrotWingsIdentityService', ['ParrotWingsCurrentIdentityService']);

parrotWingsApp.service('identityService', ['$http','currentIdentityService', function ($http, currentIdentityService) {

    var _this = this;

    this.login = function (email, password) {

        $http.get('/api/v1/identityusers/login?Email=' + email + '&Password=' + password)
        .then(function successCallback(response) {
            if (response.status == 200) {
                currentIdentityService.redirect();
            }
        });
    }

    this.register = function (userName, email, password, passwordConfirm, exceptionCallback) {
        $http.post('/api/v1/identityUsers', {
            UserName: userName,
            Email: email,
            Password: password,
            PasswordConfirm: passwordConfirm
        })
        .then(function successCallback(response) {
            currentIdentityService.redirect();
        }, function errorCallback(response) {
            //TODO: more validation ?
            if (exceptionCallback)
                exceptionCallback(response);
        });
    }
}
]);