﻿var parrotWingsApp = angular.module('ParrotWingsCurrentIdentityService', []);

parrotWingsApp.service('currentIdentityService',['$http', function ($http) {

    var _this = this;

    this.refreshLocalStorage = function () {
        $http.get('/api/v1/identityUsers/current')
        .then(function (response) {
            if (response.status == 200) {
                window.localStorage['identity'] = JSON.stringify(response.data);
            }
        });
    }

    this.get = function () {
        return JSON.parse(window.localStorage['identity']);
    }

    this.redirect = function () {
        $http.get('/api/v1/identityUsers/current')
        .then(function (response) {
            if (response.status == 200) {
                window.localStorage['identity'] = JSON.stringify(response.data);
                window.location.href = '/Inner';
            }
        });
    }

    this.logout = function () {
        $http.get('/api/v1/identityUsers/logout')
        .then(function () {
            window.location.href = '/';
            window.localStorage['identity'] = '';
        });
    };
}]);