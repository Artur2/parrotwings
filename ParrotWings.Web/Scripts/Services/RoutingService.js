﻿var parrotWingsApp = angular.module('ParrotWingsRoutingService', []);

parrotWingsApp.service('routingService', ['$location', function ($location) {

    var _this = this;

    this.changeRoute = function ($scope, url, forceReload) {
        $scope = $scope || angular.element(document).scope();
        if (forceReload || $scope.$$phase) {
            window.location = url;
        } else {
            $location.path(url);
            $scope.$apply();
        }
    }
}]);