﻿using AutoMapper;
using ParrotWings.Web.Api.DTO_s.Configuration;

namespace ParrotWings.Web.App_Start
{
    public static class MappingConfig
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg => cfg.AddProfile<MappingProfile>());
        }
    }
}