﻿using System.Web.Http;
using ParrotWings.Web.App_Start;

namespace ParrotWings.Web
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            MappingConfig.Configure();
            // Web API routes
            config.MapHttpAttributeRoutes();
            
            // Используем аттрибут роутинг
            //var apiRoute = config.Routes.MapHttpRoute(
            //    name: "DefaultApi",
            //    routeTemplate: "api/{namespace}/{controller}/{id}",
            //    defaults: new { id = RouteParameter.Optional }
            //);

            config.Formatters.Remove(config.Formatters.XmlFormatter);
        }
    }
}
