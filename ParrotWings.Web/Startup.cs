﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(ParrotWings.Web.Startup))]
namespace ParrotWings.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}