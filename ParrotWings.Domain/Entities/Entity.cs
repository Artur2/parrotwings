﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ParrotWings.Domain.Entities
{
    public abstract class Entity<TId> : IEquatable<TId>
    {
        [Key]
        public TId Id { get; set; }

        public virtual bool Equals(TId other)
        {
            return Id.Equals(other);
        }
    }
}
