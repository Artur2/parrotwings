﻿using Microsoft.AspNet.Identity;
using ParrotWings.Infrastructure.Configuration;

namespace ParrotWings.Domain.Entities.IdentityAgg
{
    [Entity]
    public class IdentityUser : Entity<string>, IUser<string>
    {
        public string UserName { get; set; }

        public string Email { get; set; }

        public string PasswordHash { get; set; }

        public decimal PWBallance { get; set; }
    }
}
