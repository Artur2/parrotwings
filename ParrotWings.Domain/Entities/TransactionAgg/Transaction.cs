﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using ParrotWings.Domain.Entities.IdentityAgg;
using ParrotWings.Infrastructure.Configuration;

namespace ParrotWings.Domain.Entities.TransactionAgg
{
    [Entity]
    public class Transaction : Entity<string>
    {
        [ForeignKey("Receiver")]
        public string ReceiverId { get; set; }

        public virtual IdentityUser Receiver { get; set; }

        [ForeignKey("Sender")]
        public string SenderId { get; set; }

        public virtual IdentityUser Sender { get; set; }

        public decimal Amount { get; set; }

        public string CorrespondentName { get; set; }

        public decimal SenderBalance { get; set; }

        public decimal ReceiverBalance { get; set; }

        public DateTime Date { get; set; }
    }
}
