﻿using ParrotWings.Domain.Entities.TransactionAgg;

namespace ParrotWings.Domain.Services
{
    public interface IPaymentService
    {
        Transaction CreateTransaction(string senderId, string receiverId, decimal amount);
    }
}
