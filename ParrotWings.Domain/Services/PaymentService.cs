﻿using System;
using System.Threading;
using ParrotWings.Domain.Entities.IdentityAgg;
using ParrotWings.Domain.Entities.TransactionAgg;
using ParrotWings.Infrastructure.Repositories;

namespace ParrotWings.Domain.Services
{
    public class PaymentService : IPaymentService
    {
        private IRepository<Transaction> _transactionsRepository;
        private IRepository<IdentityUser> _identityUsersRepository;
        private Mutex _synchronizer;

        public PaymentService(
            IRepository<Transaction> transactionsRepository,
            IRepository<IdentityUser> identityUsersRepostory)
        {
            _transactionsRepository = transactionsRepository;
            _identityUsersRepository = identityUsersRepostory;
        }

        public Transaction CreateTransaction(string senderId, string receiverId, decimal amount)
        {
            if (string.IsNullOrWhiteSpace(senderId))
                throw new ArgumentNullException("Пустое значение отправителя");
            if (string.IsNullOrWhiteSpace(receiverId))
                throw new ArgumentNullException("Пустое значение получателся");
            if (receiverId == senderId)
                throw new InvalidOperationException("Тот же отправитель и получатель");
            // Рискуем нарваться на race condition, когда считаем одновременно в 2-х потоках одного отправителя, и по минусам будет херня
            _synchronizer = new Mutex(false, "PaymentService");

            try
            {
                if (_synchronizer.WaitOne(TimeSpan.FromMilliseconds(5000)))
                {

                    var sender = _identityUsersRepository.Get(senderId);
                    var receiver = _identityUsersRepository.Get(receiverId);
                    if (sender == null || receiver == null)
                        throw new InvalidOperationException("Получатель или отправитель не найдены в базе данных");
                    if (sender.PWBallance < amount)
                        throw new InvalidOperationException("Не хватает баланса для создания транзакции с данной суммой");
                    if (amount <= 0)
                        throw new InvalidOperationException("Сумма должна быть больше 0");

                    var newTransaction = new Transaction
                    {
                        Amount = amount,
                        Date = DateTime.Now,
                        Id = Guid.NewGuid().ToString(),
                        ReceiverId = receiverId,
                        SenderId = senderId,
                        CorrespondentName = receiver.UserName
                    };
                    _transactionsRepository.Add(newTransaction);

                    sender.PWBallance -= amount;
                    receiver.PWBallance += amount;

                    newTransaction.SenderBalance = sender.PWBallance;
                    newTransaction.ReceiverBalance = receiver.PWBallance;

                    _transactionsRepository.UnitOfWork.Commit();

                    return newTransaction;
                }
                else
                {
                    throw new InvalidOperationException("Synchronization timeout");
                }
            }
            finally
            {
                _synchronizer.ReleaseMutex();
                _synchronizer.Dispose();
            }
        }
    }
}
