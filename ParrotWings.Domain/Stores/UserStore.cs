﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using ParrotWings.Domain.Entities.IdentityAgg;
using ParrotWings.Infrastructure.Repositories;

namespace ParrotWings.Domain.Stores
{
    public class UserStore : IUserStore<IdentityUser>,
        IUserEmailStore<IdentityUser>,
        IUserPasswordStore<IdentityUser>,
        IUserLockoutStore<IdentityUser, string>,
        IUserTwoFactorStore<IdentityUser,string>
    {
        private IRepository<IdentityUser> _usersRepository;

        public UserStore(IRepository<IdentityUser> usersRepository)
        {
            _usersRepository = usersRepository;
        }

        public Task CreateAsync(IdentityUser user)
        {
            _usersRepository.Add(user);
            _usersRepository.UnitOfWork.Commit();
            return Task.FromResult(0);
        }

        public Task DeleteAsync(IdentityUser user)
        {
            _usersRepository.Remove(user.Id);
            _usersRepository.UnitOfWork.Commit();
            return Task.FromResult(0);
        }

        public void Dispose()
        {
            _usersRepository.UnitOfWork.Dispose();
        }

        public Task<IdentityUser> FindByEmailAsync(string email)
        {
            return Task.FromResult(_usersRepository.AsQueryable().FirstOrDefault(user => user.Email == email));
        }

        public Task<IdentityUser> FindByIdAsync(string userId)
        {
            return Task.FromResult(_usersRepository.AsQueryable().FirstOrDefault(user => user.Id == userId));
        }

        public Task<IdentityUser> FindByNameAsync(string userName)
        {
            return Task.FromResult(_usersRepository.AsQueryable().FirstOrDefault(user => user.UserName == userName));
        }

        public Task<int> GetAccessFailedCountAsync(IdentityUser user)
        {
            return Task.FromResult(0);
        }

        public Task<string> GetEmailAsync(IdentityUser user)
        {
            return Task.FromResult(user.Email);
        }

        public Task<bool> GetEmailConfirmedAsync(IdentityUser user)
        {
            return Task.FromResult(true);
        }

        public Task<bool> GetLockoutEnabledAsync(IdentityUser user)
        {
            return Task.FromResult(false);
        }

        public Task<DateTimeOffset> GetLockoutEndDateAsync(IdentityUser user)
        {
            return Task.FromResult(DateTimeOffset.MinValue);
        }

        public Task<string> GetPasswordHashAsync(IdentityUser user)
        {
            return Task.FromResult(user.PasswordHash);
        }

        public Task<bool> GetTwoFactorEnabledAsync(IdentityUser user)
        {
            return Task.FromResult(false);
        }

        public Task<bool> HasPasswordAsync(IdentityUser user)
        {
            return Task.FromResult(!string.IsNullOrWhiteSpace(user.PasswordHash));
        }

        public Task<int> IncrementAccessFailedCountAsync(IdentityUser user)
        {
            return Task.FromResult(0);
        }

        public Task ResetAccessFailedCountAsync(IdentityUser user)
        {
            return Task.FromResult(0);
        }

        public Task SetEmailAsync(IdentityUser user, string email)
        {
            user.Email = email;
            return Task.FromResult(0);
        }

        public Task SetEmailConfirmedAsync(IdentityUser user, bool confirmed)
        {
            return Task.FromResult(0);
        }

        public Task SetLockoutEnabledAsync(IdentityUser user, bool enabled)
        {
            return Task.FromResult(0);
        }

        public Task SetLockoutEndDateAsync(IdentityUser user, DateTimeOffset lockoutEnd)
        {
            return Task.FromResult(0);
        }

        public Task SetPasswordHashAsync(IdentityUser user, string passwordHash)
        {
            user.PasswordHash = passwordHash;
            return Task.FromResult(0);
        }

        public Task SetTwoFactorEnabledAsync(IdentityUser user, bool enabled)
        {
            return Task.FromResult(0);
        }

        public Task UpdateAsync(IdentityUser user)
        {
            _usersRepository.UnitOfWork.Commit();
            return Task.FromResult(0);
        }
    }
}
